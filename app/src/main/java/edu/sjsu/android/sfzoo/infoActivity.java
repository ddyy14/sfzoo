package edu.sjsu.android.sfzoo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class infoActivity extends AppCompatActivity {

    Button call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        call = (Button)findViewById(R.id.dia_btn_call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = "tel: 555-12345";
                Intent myActivity = new Intent(Intent.ACTION_DIAL, Uri.parse(phoneNumber));
                startActivity(myActivity);
            }
        });
    }
}
