package edu.sjsu.android.sfzoo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String s1[], s2[];
    // init items
    int images[] = {R.drawable.atroce, R.drawable.beelzebub, R.drawable.deadknight, R.drawable.frog, R.drawable.hpmvp, R.drawable.lordknight};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = findViewById(R.id.recyclerView);

        s1 = getResources().getStringArray(R.array.ragnarok);
        s2 = getResources().getStringArray(R.array.description);

        RecyclerAdpter recyclerAdpter = new RecyclerAdpter(this, s1, s2, images);
        recyclerView.setAdapter(recyclerAdpter);
       recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info:
                Intent newActivity = new Intent(MainActivity.this, infoActivity.class);
                startActivity(newActivity);
                return true;
            case R.id.uninstall:
                Uri packageUri = Uri.parse("package:edu.sjsu.android.sfzoo");
                Intent uninstallintent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
                startActivity(uninstallintent);
                return true;
                default:
            return super.onOptionsItemSelected(item);
        }
    }


}
